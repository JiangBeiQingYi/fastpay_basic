package cn.fastpay.core.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;

/**
 * 权限基类
 * @author freewolf
 */
@MappedSuperclass
public abstract class BaseObject implements Serializable {

    private static final long serialVersionUID = -6055185304239599286L;
    /**
     * 主键ID.
     */
    @Id
    @GeneratedValue
    private Long id;
    /**
     * 版本号 - 默认为0.
     */
    private Integer version;
    /**
     * 状态 PublicStatusEnum
     */
    private String status;
    /**
     * 创建者
     */
    private String creator;
    /**
     * 创建时间.
     */
    private Date createTime;
    /**
     * 修改人.
     */
    private String editor;
    /**
     * 修改时间.
     */
    private Date editTime;
    /**
     * 描述
     */
    private String remark;
    /**
     * 已经删除
     */
    @Column(columnDefinition="tinyint(1) default 0")
    private Boolean deleted;

    public BaseObject() {
        this.version = 0;
        this.deleted = false;
        this.createTime = new Date();
    }

    @Override
    public String toString() {
        return "BaseObject{" +
                "id=" + id +
                ", version=" + version +
                ", status='" + status + '\'' +
                ", creator='" + creator + '\'' +
                ", createTime=" + createTime +
                ", editor='" + editor + '\'' +
                ", editTime=" + editTime +
                ", remark='" + remark + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public Date getEditTime() {
        return editTime;
    }

    public void setEditTime(Date editTime) {
        this.editTime = editTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
