
package cn.fastpay.core.exception;

/**
 * 业务异常基类，所有业务异常都必须继承于此异常 .
 * @author freewolf
 */
public class BaseException extends RuntimeException {

    private static final long serialVersionUID = -5875371379845226068L;

    /**
     * 未知错误
     */
    public static final BaseException UNKNOWN_ERROR = new BaseException(
            10000001, "未知错误");
    /**
     * 插入数据不能包含ID
     */
    public static final BaseException DB_INSERT_INCLUDE_ID = new BaseException(
            10040008, "插入数据不能包含ID");
    /**
     * 更新数据不能无ID
     */
    public static final BaseException DB_UPDATE_NOT_INCLUDE_ID = new BaseException(
            10040009, "更新数据不能无ID");

    /**
     * 数据库操作,insert返回0
     */
    public static final BaseException DB_INSERT_RESULT_0 = new BaseException(
            10040001, "数据库操作,insert返回0");

    /**
     * 数据库操作,update返回0
     */
    public static final BaseException DB_UPDATE_RESULT_0 = new BaseException(
            10040002, "数据库操作,update返回0");

    /**
     * 数据库操作,selectOne返回null
     */
    public static final BaseException DB_SELECTONE_IS_NULL = new BaseException(
            10040003, "数据库操作,selectOne返回null");

    /**
     * 数据库操作,list返回null
     */
    public static final BaseException DB_LIST_IS_NULL = new BaseException(
            10040004, "数据库操作,list返回null");

    /**
     * Token 验证不通过
     */
    public static final BaseException TOKEN_IS_ILLICIT = new BaseException(
            10040005, "Token 验证非法");
    /**
     * 会话超时　获取session时，如果是空，throws 下面这个异常 拦截器会拦截爆会话超时页面
     */
    public static final BaseException SESSION_IS_OUT_TIME = new BaseException(
            10040006, "会话超时");

    /**
     * 生成序列异常时
     */
    public static final BaseException DB_GET_SEQ_NEXT_VALUE_ERROR = new BaseException(
            10040007, "序列生成超时");

    /**
     * 生成序列异常时
     */
    public static final BaseException OBJECT_TO_MAP = new BaseException(
            10040008, "对象转换Map时失败");

    /**
     * 异常信息
     */
    protected String msg;

    /**
     * 具体异常码
     */
    protected int code;

    public BaseException(int code, String msgFormat, Object... args) {
        super(String.format(msgFormat, args));
        this.code = code;
        this.msg = String.format(msgFormat, args);
    }

    public BaseException() {
        super();
    }

    public BaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public BaseException(Throwable cause) {
        super(cause);
    }

    public BaseException(String message) {
        super(message);
    }

    public String getMsg() {
        return msg;
    }

    public int getCode() {
        return code;
    }

    /**
     * 实例化异常
     * 
     * @param msgFormat
     * @param args
     * @return
     */
    public BaseException newInstance(String msgFormat, Object... args) {
        return new BaseException(this.code, msgFormat, args);
    }

}
