package cn.fastpay.core.handler;

import cn.fastpay.core.exception.BaseException;
import cn.fastpay.core.vo.ApiCommonResultVO;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常统一处理
 * @author freewolf
 */
public class GlobalExceptionHandler {

    @ExceptionHandler(value = BaseException.class)
    public ApiCommonResultVO handleISSException(BaseException e) {
        return new ApiCommonResultVO((long) e.getCode(), e.getMessage());
    }

    @ExceptionHandler(value = Exception.class)
    public ApiCommonResultVO exception(Exception e) {
        e.printStackTrace();
        return new ApiCommonResultVO(-1L, "未知错误");
    }

}
