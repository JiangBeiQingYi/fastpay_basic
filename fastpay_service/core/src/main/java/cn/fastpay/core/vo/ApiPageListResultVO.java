/*
 * Copyright 2015-2102 Fast(http://www.cloudate.net) Group.
 *  
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.fastpay.core.vo;

import org.springframework.data.domain.Page;

/**
 * API请求,返回分页数据时,统一实体类,将返回的数据统一封装到该实体中,返回给客户端
 */
public class ApiPageListResultVO {

    /**
     * 返回码
     */
    private int code;

    /**
     *  返回描述
     */
    private String message = "";

    /**
     *  返回分页数据,默认为0页0条
     */
    private PageVO data;

    public ApiPageListResultVO(Page page) {
        this.code = 0;
        this.data = new PageVO(page.getTotalElements(), page.getTotalPages(), page.getSize(), page.getContent());
    }

    public ApiPageListResultVO(PageVO data) {
        this.code = 0;
        this.data = data;
    }

    @Override
    public String toString() {
        return "ApiPageListResultVO{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setData(PageVO data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public PageVO getData() {
        return data;
    }

}
