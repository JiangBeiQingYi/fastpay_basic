/*
 * Copyright 2015-2102 Fast(http://www.cloudate.net) Group.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.fastpay.core.vo;


import cn.fastpay.core.exception.BaseException;
import cn.fastpay.core.util.MapUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * api请求正常返回结果,该实体作为API请求时,按照规范返回的实体.
 * code 为返回码
 * message 为返回描述
 * data 为返回的具体结果
 *
 * @author freewolf
 */
public class ApiCommonResultVO {

    public ApiCommonResultVO(Long code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = new HashMap<>();
        if (data != null) {
            try {
                this.data = MapUtil.Obj2Map(data);
            } catch (Exception e) {
                throw BaseException.OBJECT_TO_MAP;
            }
        }
    }

    public ApiCommonResultVO(Long code, String message) {
        this.code = code;
        this.message = message;
        this.data = new HashMap<>();
    }

    public ApiCommonResultVO(Object data) {
        this.code = 0L;
        this.message = "";

        if (data != null && data instanceof HashMap) {
            this.data = (HashMap) data;
        } else if (data != null) {
            this.data = new HashMap<>();
            try {
                this.data = MapUtil.Obj2Map(data);
            } catch (Exception e) {
                throw BaseException.OBJECT_TO_MAP;
            }
        }
    }


    public static ApiCommonResultVO success() {
        return new ApiCommonResultVO(0L, "成功");
    }

    public ApiCommonResultVO(String key, Object value) {
        this.code = 0L;
        this.message = "";
        this.data = new HashMap<>();

        this.data.put(key, value);
    }

    /**
     * 返回码
     */
    private Long code;

    /**
     * 返回描述
     */
    private String message;

    /**
     * 返回数据
     */
    private Map<String, Object> data;

    public void setCode(Long code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public Long getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public Object getData() {
        return data;
    }

}
