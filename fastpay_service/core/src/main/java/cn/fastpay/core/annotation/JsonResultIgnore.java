package cn.fastpay.core.annotation;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface JsonResultIgnore {
    String[] keys() default {};
}
