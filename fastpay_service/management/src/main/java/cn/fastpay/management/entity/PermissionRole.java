package cn.fastpay.management.entity;

import cn.fastpay.core.entity.BaseObject;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * 权限角色
 * @author freewolf
 */
@Entity
public class PermissionRole extends BaseObject {

    private static final long serialVersionUID = 7954215785057211821L;
    /**
     * 角色编码：例如：admin
     */
    private String role;
    /**
     * 角色名称
     */
    private String roleName;
    /**
     * 角色对应的操作
     */
    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinTable(name = "permission_role_action", joinColumns = { @JoinColumn(name = "role_id") },
            inverseJoinColumns = {@JoinColumn(name = "action_id") })
    private Set<PermissionAction> actionSet;
    /**
     * 角色对应的菜单
     */
    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinTable(name = "permission_role_menu", joinColumns = { @JoinColumn(name = "role_id") },
            inverseJoinColumns = {@JoinColumn(name = "menu_id") })
    private Set<PermissionMenu> menuSet;

    public PermissionRole() {
        this.menuSet = new HashSet<>();
        this.actionSet = new HashSet<>();
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Set<PermissionAction> getActionSet() {
        return actionSet;
    }

    public void setActionSet(Set<PermissionAction> actionSet) {
        this.actionSet = actionSet;
    }

    public Set<PermissionMenu> getMenuSet() {
        return menuSet;
    }

    public void setMenuSet(Set<PermissionMenu> menuSet) {
        this.menuSet = menuSet;
    }
}

