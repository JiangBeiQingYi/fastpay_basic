package cn.fastpay.management.exception;


import cn.fastpay.core.exception.BaseException;

/**
 * @author freewolf
 */
public class PermissionOperatorException {
    public static final BaseException OPERATOR_PASSWORD_WRONG = new BaseException(
            10010001, "账号不存在或者密码错误!");
    public static final BaseException OPERATOR_LOCKED = new BaseException(
            10010002, "该操作员账号被锁定!");
    public static final BaseException OPERATOR_NOT_FOUND = new BaseException(
            10010003, "该操作员未找到!");
    public static final BaseException MENU_REMVOE_CONSTRAINT = new BaseException(
            10010004, "菜单包含级联数据，删除需要无用户使用该菜单!");
}
