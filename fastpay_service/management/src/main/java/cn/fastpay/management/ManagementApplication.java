package cn.fastpay.management;

import cn.fastpay.core.exception.BaseException;
import cn.fastpay.core.util.ErrorUtil;
import cn.fastpay.management.entity.*;
import cn.fastpay.management.entity.PermissionAction;
import cn.fastpay.management.entity.PermissionOperator;
import cn.fastpay.management.service.PermissionOperatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.DefaultErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.context.request.RequestAttributes;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author freewolf
 */
@SpringBootApplication
public class ManagementApplication implements CommandLineRunner {

    @Autowired
    PermissionOperatorService optatorService;
    @Value("${setting.security.jwt.expire-time}")
    private String JWT_EXPIRE_TIME;
    @Value("${setting.security.jwt.secret}")
    private String JWT_SECRET;

    public static void main(String[] args) {
        SpringApplication.run(ManagementApplication.class, args);
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public ErrorAttributes errorAttributes() {
        return new DefaultErrorAttributes() {
            @Override
            public Map<String, Object> getErrorAttributes(RequestAttributes requestAttributes, boolean includeStackTrace) {
                Map<String, Object> errorAttributes = super.getErrorAttributes(requestAttributes, includeStackTrace);
                return ErrorUtil.getErrorAttributes(errorAttributes, this, requestAttributes, includeStackTrace);
            }
        };
    }

    @Override
    public void run(String... strings) throws Exception {
        PermissionOperator admin = new PermissionOperator();
        admin.setLoginName("admin");
        admin.setRealName("张三");
        admin.setMobile("13901234567");
        admin.setLoginPassword("123456");
        admin.setAccountNonLocked(false);
        admin.setAccountNonLocked(true);

        PermissionRole roleAdmin = new PermissionRole();
        roleAdmin.setRole("role_admin");
        roleAdmin.setRoleName("管理员");

        Set<PermissionRole> roleSet = new HashSet<>();
        roleSet.add(roleAdmin);

        PermissionAction action = new PermissionAction();
        action.setPermission("action_change_password");
        action.setPermissionName("修改密码");

        Set<PermissionAction> actionSet = new HashSet<>();
        actionSet.add(action);

        // 菜单
        PermissionMenu menuHome = new PermissionMenu();
        menuHome.setLevel(0L);
        menuHome.setName("首页");
        menuHome.setLeaf(true);
        menuHome.setSort(100L);
        menuHome.setPath("/");
        menuHome.setTemplate("/Layout.vue");
        menuHome.setIcon("el-icon-menu");
        menuHome.setParent(null);

        PermissionMenu homeMenu = new PermissionMenu();
        homeMenu.setLevel(1L);
        homeMenu.setSort(90L);
        homeMenu.setName("~");
        homeMenu.setTemplate("/Index.vue");
        homeMenu.setPath("/home");
        homeMenu.setIcon("el-icon-document");
        homeMenu.setParent(menuHome);

        PermissionMenu menu1 = new PermissionMenu();
        menu1.setLevel(0L);
        menu1.setSort(90L);
        menu1.setName("菜单1");
        menu1.setLeaf(false);
        menu1.setTemplate("/Layout.vue");
        menu1.setPath("/test");
        menu1.setIcon("el-icon-document");
        menu1.setParent(null);

        PermissionMenu menu2 = new PermissionMenu();
        menu2.setLevel(1L);
        menu2.setSort(90L);
        menu2.setName("菜单2");
        menu2.setTemplate("/system/Demo.vue");
        menu2.setTemplate("/system/Demo.vue");
        menu2.setPath("/homes");
        menu2.setIcon("el-icon-document");
        menu2.setParent(menu1);


        PermissionMenu sysMenu = new PermissionMenu();
        sysMenu.setLevel(0L);
        sysMenu.setSort(80L);
        sysMenu.setName("系统管理");
        sysMenu.setLeaf(false);
        sysMenu.setTemplate("/Layout.vue");
        sysMenu.setPath("/system/");
        sysMenu.setIcon("el-icon-setting");
        sysMenu.setParent(null);

        PermissionMenu adminMenu = new PermissionMenu();
        adminMenu.setLevel(1L);
        adminMenu.setName("管路员");
        adminMenu.setTemplate("/system/Operator.vue");
        adminMenu.setPath("/system/operators");
        adminMenu.setIcon("el-icon-location");
        adminMenu.setParent(sysMenu);

        PermissionMenu roleMenu = new PermissionMenu();
        roleMenu.setLevel(1L);
        roleMenu.setName("角色");
        roleMenu.setTemplate("/system/Role.vue");
        roleMenu.setPath("/system/roles");
        roleMenu.setIcon("el-icon-location");
        roleMenu.setParent(sysMenu);

        PermissionMenu actionPermission = new PermissionMenu();
        actionPermission.setLevel(1L);
        actionPermission.setName("权限");
        actionPermission.setTemplate("/system/Permission.vue");
        actionPermission.setPath("/system/permission");
        actionPermission.setIcon("el-icon-location");
        actionPermission.setParent(sysMenu);

        PermissionMenu actionMenu = new PermissionMenu();
        actionMenu.setLevel(1L);
        actionMenu.setName("菜单");
        actionMenu.setTemplate("/system/Menu.vue");
        actionMenu.setPath("/system/menus");
        actionMenu.setIcon("el-icon-location");
        actionMenu.setParent(sysMenu);

        Set<PermissionMenu> menuSet = new HashSet<>();
        menuSet.add(menuHome);
        menuSet.add(menu1);
        menuSet.add(menu2);
        menuSet.add(sysMenu);
        menuSet.add(adminMenu);
        menuSet.add(roleMenu);
        menuSet.add(actionMenu);
        menuSet.add(actionPermission);
        menuSet.add(homeMenu);

        roleAdmin.setActionSet(actionSet);
        roleAdmin.setMenuSet(menuSet);
        admin.setRoleSet(roleSet);

        optatorService.add(admin);

    }
}

