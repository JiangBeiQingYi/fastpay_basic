package cn.fastpay.management.repository;

import cn.fastpay.management.entity.PermissionMenu;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * @author freewolf
 */
public interface PermissionMenuRepository extends JpaRepository<PermissionMenu, Long> {

}
