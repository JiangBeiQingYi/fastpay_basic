package cn.fastpay.management.service;


import cn.fastpay.management.entity.PermissionMenu;

import java.util.List;

/**
 * @author freewolf
 */
public interface PermissionMenuService {

    /**
     * 根据ID删除一个操作员，同时删除与该操作员关联的角色关联信息. type="admin"的超级管理员不能删除.
     *
     * @param id
     * 操作员ID.
     */
    void deleteById(Long id);

    /**
     * 创建 PermissionMenu
     */
    void add(PermissionMenu menu);

    /**
     * 修改 PermissionMenu
     */
    void update(PermissionMenu menu);

    /**
     * 根据id获取数据 PermissionMenu
     *
     * @param id
     *
     * @return
     */
    PermissionMenu getById(Long id);

    /**
     * 获取所有
     * @return
     */
    List<PermissionMenu> getAll();

}
