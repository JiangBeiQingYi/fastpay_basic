package cn.fastpay.management.entity;

import cn.fastpay.core.entity.BaseObject;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 * 权限操作
 *
 * @author freewolf
 */
@Entity
@Where(clause = "deleted = 0")
@Table(name = "permission_action")
public class PermissionAction extends BaseObject {
    private static final long serialVersionUID = -7155395714211204911L;
    /**
     * 权限名称
     */
    private String permissionName;
    /**
     * 权限标识
     */
    private String permission;

    @JsonIgnore
    @ManyToMany(mappedBy = "actionSet")
    private Set<PermissionRole> roleSet;

    public PermissionAction() {
        this.roleSet = new HashSet<>();
    }

    public Set<PermissionRole> getRoleSet() {
        return roleSet;
    }

    public void setRoleSet(Set<PermissionRole> roleSet) {
        this.roleSet = roleSet;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }
}
