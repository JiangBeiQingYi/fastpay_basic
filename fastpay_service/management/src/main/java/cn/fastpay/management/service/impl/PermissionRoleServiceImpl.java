package cn.fastpay.management.service.impl;

import cn.fastpay.core.exception.BaseException;
import cn.fastpay.management.entity.PermissionMenu;
import cn.fastpay.management.entity.PermissionRole;
import cn.fastpay.management.repository.PermissionMenuRepository;
import cn.fastpay.management.repository.PermissionRoleRepository;
import cn.fastpay.management.service.PermissionRoleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * @author freewolf
 */
@Service
public class PermissionRoleServiceImpl implements PermissionRoleService {

    @Autowired
    private PermissionRoleRepository permissionRoleRepository;

    @Autowired
    private PermissionMenuRepository permissionMenuRepository;

    @Override
    public Page<PermissionRole> getPage(int page, int pageSize) {
        Sort sort = new Sort(Sort.Direction.DESC,"createTime");
        Pageable pageable = new PageRequest(page, pageSize, sort);
        return this.permissionRoleRepository.findAll(pageable);
    }

    @Override
    public void deleteById(Long id) {
        this.permissionRoleRepository.delete(id);
    }

    @Override
    public void add(PermissionRole role) {
        if (role.getId() != null) {
            throw BaseException.DB_INSERT_INCLUDE_ID;
        } else {
            List<Long> menusIds = new ArrayList<>();
            role.getMenuSet().forEach(menu -> menusIds.add(menu.getId()));
            List<PermissionMenu> permissionMenus = this.permissionMenuRepository.findAll(menusIds);
            role.setMenuSet(new HashSet<>(permissionMenus));
            this.permissionRoleRepository.save(role);
        }
    }

    @Override
    public void update(PermissionRole role) {
        PermissionRole permissionRole = this.permissionRoleRepository.findOne(role.getId());
        if(permissionRole == null) {
            throw BaseException.DB_UPDATE_NOT_INCLUDE_ID;
        }
        permissionRole.getMenuSet().clear();
        permissionRole.setRole(role.getRole());
        permissionRole.setRoleName(role.getRoleName());
        permissionRole.setMenuSet(role.getMenuSet());
        this.permissionRoleRepository.save(permissionRole);
    }

    @Override
    public void updatePermissionAction(PermissionRole permissionRole) {
        PermissionRole role = this.permissionRoleRepository.findOne(permissionRole.getId());
        if (role == null) {
            throw BaseException.DB_UPDATE_NOT_INCLUDE_ID;
        }
        role.getActionSet().clear();
        role.setActionSet(permissionRole.getActionSet());
        this.permissionRoleRepository.save(role);
    }

    @Override
    public PermissionRole getById(Long id) {
        return this.permissionRoleRepository.findOne(id) ;
    }

    @Override
    public List<PermissionRole> findAll() {
        return this.permissionRoleRepository.findAll();
    }
}
