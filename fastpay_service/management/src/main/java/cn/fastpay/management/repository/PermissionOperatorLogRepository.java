package cn.fastpay.management.repository;

import cn.fastpay.management.entity.PermissionOperatorLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PermissionOperatorLogRepository extends JpaRepository<PermissionOperatorLog, Long> {
}
