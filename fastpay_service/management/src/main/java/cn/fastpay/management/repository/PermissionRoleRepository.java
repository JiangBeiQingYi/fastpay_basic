package cn.fastpay.management.repository;

import cn.fastpay.management.entity.PermissionRole;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author freewolf
 */
public interface PermissionRoleRepository extends JpaRepository<PermissionRole, Long> {
}
