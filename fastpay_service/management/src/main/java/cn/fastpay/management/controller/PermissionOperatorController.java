package cn.fastpay.management.controller;

import cn.fastpay.core.annotation.JsonResultIgnore;
import cn.fastpay.core.exception.BaseException;
import cn.fastpay.core.page.PageParam;
import cn.fastpay.core.vo.ApiCommonResultVO;
import cn.fastpay.core.vo.ApiPageListResultVO;
import cn.fastpay.management.entity.PermissionOperator;
import cn.fastpay.management.service.PermissionOperatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author freewolf
 */
@RestController
@RequestMapping(value = "/api/operator")
public class PermissionOperatorController {
    @Autowired
    private PermissionOperatorService permissionOperatorService;

    @PostMapping("/info")
    @JsonResultIgnore(keys = {"loginPassword"})
    ApiCommonResultVO getOperator() {
        String loginName = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        PermissionOperator operator = this.permissionOperatorService.getByLoginName(loginName);
        ApiCommonResultVO resultVO = new ApiCommonResultVO(operator);
        return resultVO;
    }

    @PostMapping("/page")
    ApiPageListResultVO getOperatorPage(@RequestBody PageParam pageParam) {
        Page<PermissionOperator> page = this.permissionOperatorService.getPage(pageParam.getPage(), pageParam.getPageSize());
        return new ApiPageListResultVO(page);
    }

    @PostMapping("/add")
    ApiCommonResultVO save(@RequestBody PermissionOperator operator) {
        if (operator.getId() == null) {
            PermissionOperator updateOperator = new PermissionOperator();
            updateOperator.setLoginName(operator.getLoginName());
            updateOperator.setLoginPassword(operator.getLoginPassword());
            updateOperator.setRealName(operator.getRealName());
            updateOperator.setMobile(operator.getMobile());
            this.permissionOperatorService.add(updateOperator);
        }

        return ApiCommonResultVO.success();
    }

    @PostMapping("/update")
    ApiCommonResultVO update(@RequestBody PermissionOperator operator) {
        if (operator.getId() != null) {
            PermissionOperator updateOperator = this.permissionOperatorService.getById(operator.getId());
            if (updateOperator != null) {
                updateOperator.setMobile(operator.getMobile());
                updateOperator.setLoginName(operator.getLoginName());
                updateOperator.setRealName(operator.getRealName());
                this.permissionOperatorService.update(updateOperator);
            }
        }

        return ApiCommonResultVO.success();
    }

    @PostMapping("/update/role")
    public ApiCommonResultVO updateRole(@RequestBody PermissionOperator operator) {
        if (operator.getId() == null) {
            throw BaseException.DB_UPDATE_NOT_INCLUDE_ID;
        }
        this.permissionOperatorService.updateOperatorRole(operator);
        return ApiCommonResultVO.success();
    }

    @PostMapping("/lock")
    ApiCommonResultVO lock(@RequestBody PermissionOperator operator) {
        if (operator.getId() != null) {
            PermissionOperator updateOperator = this.permissionOperatorService.getById(operator.getId());
            if (updateOperator != null) {
                updateOperator.setAccountNonLocked(!updateOperator.getAccountNonLocked());
                this.permissionOperatorService.update(updateOperator);
            }
        }

        return ApiCommonResultVO.success();
    }
}
