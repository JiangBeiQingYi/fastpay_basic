package cn.fastpay.management.service;

import cn.fastpay.management.entity.PermissionRole;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author freewolf
 */
public interface PermissionRoleService {

    /**
     * 获取 角色 分页
     *
     * @param page 页码
     *
     * @param pageSize
     *
     * @return
     */
    Page<PermissionRole> getPage(int page, int pageSize) ;

    /**
     * 根据ID删除一个操作员，同时删除与该操作员关联的角色关联信息. type="admin"的超级管理员不能删除.
     *
     * @param id
     * 操作员ID.
     */
    void deleteById(Long id);

    /**
     * 创建 PermissionAction
     */
    void add(PermissionRole permissionRole);

    /**
     * 修改 PermissionAction
     */
    void update(PermissionRole permissionRole);

    /**
     * 修改权限
     * @param permissionRole 权限
     */
    void updatePermissionAction(PermissionRole permissionRole);

    /**
     * 根据id获取数据 PermissionAction
     *
     * @param id
     *
     * @return
     */
    PermissionRole getById(Long id);

    /**
     * 查询所有角色
     * @return 所有角色
     */
    List<PermissionRole> findAll();
}
