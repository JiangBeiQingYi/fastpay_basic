package cn.fastpay.management.service;


import cn.fastpay.management.entity.PermissionAction;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author freewolf
 */
public interface PermissionActionService {

    /**
     * 获取操作员分页
     *
     * @param page 页码
     *
     * @param pageSize
     *
     * @return
     */
    Page<PermissionAction> getPage(int page, int pageSize);

    /**
     * 根据ID删除一个操作员，同时删除与该操作员关联的角色关联信息. type="admin"的超级管理员不能删除.
     *
     * @param id
     * 操作员ID.
     */
    public void deleteById(Long id);

    /**
     * 创建 PermissionAction
     */
    void add(PermissionAction permissionAction);

    /**
     * 修改 PermissionAction
     */
    void update(PermissionAction permissionAction);

    /**
     * 根据id获取数据 PermissionAction
     *
     * @param id
     *
     * @return
     */
    PermissionAction getById(Long id);

    /**
     * 获得全部的 PermissionAction
     *
     * @return
     */
     List<PermissionAction> getAll();
}
