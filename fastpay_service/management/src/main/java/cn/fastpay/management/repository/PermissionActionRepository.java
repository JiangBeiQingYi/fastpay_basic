package cn.fastpay.management.repository;

import cn.fastpay.management.entity.PermissionAction;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author freewolf
 */
public interface PermissionActionRepository extends JpaRepository<PermissionAction, Long> {
}
