package cn.fastpay.management.entity;

import cn.fastpay.core.entity.BaseObject;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * 权限菜单
 * @author freewolf
 */
@Entity
@Where(clause = "deleted = 0")
public class PermissionMenu extends BaseObject {

    private static final long serialVersionUID = 7384806754324889052L;
    /**
     * 菜单名称
     */
    private String name;
    private String template;
    /**
     * 菜单地址
     */
    private String path;
    /**
     * 菜单编号（用于显示时排序）
     */
    private Long sort;
    /**
     * 是否为叶子节点
     */
    @Column(columnDefinition="tinyint(1) default 1")
    private Boolean leaf;
    /**
     * 隐藏
     */
    @Column(columnDefinition="tinyint(1) default 0")
    private Boolean hidden;
    /**
     * 菜单层级
     */
    private Long level;
    /**
     * 父菜单
     */
    @ManyToOne(cascade = { CascadeType.ALL })
    @JoinColumn(name="parent_id")
    private PermissionMenu parent;

    private String icon;

    @JsonIgnore
    @ManyToMany(mappedBy = "menuSet")
    private Set<PermissionRole> roleSet;

    public PermissionMenu() {
        this.leaf = false;
        this.hidden = false;
        this.sort = 0L;
        this.level = 0L;
        this.roleSet = new HashSet<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getSort() {
        return sort;
    }

    public void setSort(Long sort) {
        this.sort = sort;
    }

    public Boolean getLeaf() {
        return leaf;
    }

    public void setLeaf(Boolean leaf) {
        this.leaf = leaf;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public PermissionMenu getParent() {
        return parent;
    }

    public void setParent(PermissionMenu parent) {
        this.parent = parent;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public Set<PermissionRole> getRoleSet() {
        return roleSet;
    }

    public void setRoleSet(Set<PermissionRole> roleSet) {
        this.roleSet = roleSet;
    }
}
