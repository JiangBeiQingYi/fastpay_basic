package cn.fastpay.management.controller;

import cn.fastpay.core.exception.BaseException;
import cn.fastpay.management.entity.PermissionRole;
import cn.fastpay.management.service.PermissionActionService;
import cn.fastpay.management.service.PermissionMenuService;
import cn.fastpay.management.service.PermissionRoleService;
import cn.fastpay.core.page.PageParam;
import cn.fastpay.core.vo.ApiCommonResultVO;
import cn.fastpay.core.vo.ApiPageListResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author freewolf
 */
@RestController
@RequestMapping(value = "/api/role")
public class PermissionRoleController {

    @Autowired
    private PermissionRoleService permissionRoleService;
    @Autowired
    private PermissionActionService permissionActionService;
    @Autowired
    private PermissionMenuService permissionMenuService;


    @PostMapping("/page")
    private ApiPageListResultVO page(@RequestBody PageParam pageParam) {
        Page<PermissionRole> page = this.permissionRoleService.getPage(pageParam.getPage(), pageParam.getPageSize());
        return new ApiPageListResultVO(page);
    }

    @GetMapping("/all")
    public ApiCommonResultVO findAll() {
        List<PermissionRole> permissionRoles = this.permissionRoleService.findAll();
        Map<String, Object> result = new HashMap<>();
        result.put("all", permissionRoles);
        return new ApiCommonResultVO(result);
    }

    @PostMapping("/add")
    private ApiCommonResultVO add(@RequestBody PermissionRole role) {
        if (role.getId() == null) {
            PermissionRole updateRole = new PermissionRole();
            updateRole.setRole(role.getRole());
            updateRole.setRoleName(role.getRoleName());
            updateRole.setMenuSet(role.getMenuSet());
            this.permissionRoleService.add(updateRole);
        }

        return ApiCommonResultVO.success();
    }

    @PostMapping("/update")
    private ApiCommonResultVO update(@RequestBody PermissionRole role) {
        if (role.getId() == null) {
            throw BaseException.DB_UPDATE_NOT_INCLUDE_ID;
        }
        this.permissionRoleService.update(role);
        return ApiCommonResultVO.success();
    }

    @PostMapping("/update/action")
    public ApiCommonResultVO updatePermissionAction(@RequestBody PermissionRole role) {
        if (role.getId() == null) {
            throw BaseException.DB_UPDATE_NOT_INCLUDE_ID;
        }
        this.permissionRoleService.updatePermissionAction(role);
        return ApiCommonResultVO.success();
    }

    @PostMapping("/remove")
    private ApiCommonResultVO remove(@RequestBody PermissionRole role) {

        PermissionRole removeRole = this.permissionRoleService.getById(role.getId());
        removeRole.getMenuSet().clear();
        removeRole.getActionSet().clear();
        this.permissionRoleService.update(removeRole);
        this.permissionRoleService.deleteById(removeRole.getId());

        return ApiCommonResultVO.success();
    }

    @PostMapping("/action/{id}")
    private ApiCommonResultVO roleAction(@PathVariable(value = "id") Long id) {

        PermissionRole role = this.permissionRoleService.getById(id);
        Map<String, Object> result = new HashMap<>();
        result.put("all", this.permissionActionService.getAll());
        result.put("selected", role.getActionSet());

        return new ApiCommonResultVO(result);
    }

    @PostMapping("/menu/{id}")
    private ApiCommonResultVO roleMenu(@PathVariable(value = "id") Long id) {

        PermissionRole role = this.permissionRoleService.getById(id);
        Map<String, Object> result = new HashMap<>();
        result.put("all", this.permissionMenuService.getAll());
        result.put("selected", role.getMenuSet());

        return new ApiCommonResultVO(result);
    }
}
