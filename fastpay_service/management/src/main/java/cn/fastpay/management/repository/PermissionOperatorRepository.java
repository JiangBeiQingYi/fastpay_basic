package cn.fastpay.management.repository;

import cn.fastpay.management.entity.PermissionOperator;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author freewolf
 */
public interface PermissionOperatorRepository extends JpaRepository<PermissionOperator, Long> {
    /**
     * 根据 登录名 查找 {@link PermissionOperator}
     * @param loginName
     * @return
     */
    PermissionOperator getByLoginName(String loginName);
}
