package cn.fastpay.management.controller;

import cn.fastpay.management.entity.PermissionAction;
import cn.fastpay.management.entity.PermissionRole;
import cn.fastpay.management.service.PermissionActionService;
import cn.fastpay.management.service.PermissionRoleService;
import cn.fastpay.core.page.PageParam;
import cn.fastpay.core.vo.ApiCommonResultVO;
import cn.fastpay.core.vo.ApiPageListResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author freewolf
 */
@RestController
@RequestMapping(value = "/api/action")
public class PermissionActionController {

    @Autowired
    private PermissionActionService permissionActionService;
    @Autowired
    private PermissionRoleService permissionRoleService;


    @PostMapping("/page")
    private ApiPageListResultVO page(@RequestBody PageParam pageParam) {
        Page<PermissionAction> page = this.permissionActionService.getPage(pageParam.getPage(), pageParam.getPageSize());
        return new ApiPageListResultVO(page);
    }

    @GetMapping("/all")
    public ApiCommonResultVO all() {
        List<PermissionAction> permissionActions = this.permissionActionService.getAll();
        Map<String, Object> result = new HashMap<>();
        result.put("all", permissionActions);
        return new ApiCommonResultVO(result);
    }

    @PostMapping("/add")
    private ApiCommonResultVO add(@RequestBody PermissionAction action) {
        if (action.getId() == null) {
            PermissionAction updateAction = new PermissionAction();
            updateAction.setPermission(action.getPermission());
            updateAction.setPermissionName(action.getPermissionName());
            this.permissionActionService.add(updateAction);
        }

        return ApiCommonResultVO.success();
    }

    @PostMapping("/update")
    private ApiCommonResultVO update(@RequestBody PermissionAction action) {
        if (action.getId() != null) {
            PermissionAction updateAction = this.permissionActionService.getById(action.getId());
            if (updateAction != null) {
                updateAction.setPermission(action.getPermission());
                updateAction.setPermissionName(action.getPermissionName());
                this.permissionActionService.update(updateAction);
            }
        }

        return ApiCommonResultVO.success();
    }

    @PostMapping("/remove")
    private ApiCommonResultVO remove(@RequestBody PermissionAction action) {
        PermissionAction removeAction = this.permissionActionService.getById(action.getId());
        for (PermissionRole role : removeAction.getRoleSet()) {
            role.getActionSet().remove(removeAction);
            this.permissionRoleService.update(role);
        }
        this.permissionActionService.deleteById(removeAction.getId());

        return ApiCommonResultVO.success();
    }
}
