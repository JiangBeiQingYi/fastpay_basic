package cn.fastpay.management.entity;


import cn.fastpay.core.entity.BaseObject;

import javax.persistence.Entity;

/**
 * 权限操作Log
 * @author freewolf
 */
@Entity
public class PermissionOperatorLog extends BaseObject {

    private static final long serialVersionUID = 9083508601549046683L;
    /**
     * 操作员ID
     */
    private Long operatorId;
    /**
     * 操作员登录名
     */
    private String operatorName;
    /**
     * 操作类型（参与枚举:OperatorLogTypeEnum,1:增加,2:修改,3:删除,4:查询,5:登录）
     */
    private String operateType;
    /**
     * IP地址
     */
    private String ip;
    /**
     * 操作内容
     */
    private String content;

    public PermissionOperatorLog() {
    }

    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getOperateType() {
        return operateType;
    }

    public void setOperateType(String operateType) {
        this.operateType = operateType;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
