package cn.fastpay.management.service.impl;

import cn.fastpay.core.exception.BaseException;
import cn.fastpay.management.entity.PermissionOperator;
import cn.fastpay.management.repository.PermissionOperatorRepository;
import cn.fastpay.management.service.PermissionOperatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * {@link PermissionOperatorService} 实现类
 * @author freewolf
 */
@Service
public class PermissionOperatorServiceImpl implements PermissionOperatorService {

    @Autowired
    private PermissionOperatorRepository permissionOperatorRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    @Override
    public void add(PermissionOperator operator) {
        if (operator.getId() != null) {
            throw BaseException.DB_INSERT_INCLUDE_ID;
        } else {
            operator.setLoginPassword(bCryptPasswordEncoder.encode(operator.getLoginPassword().trim()));
            permissionOperatorRepository.save(operator);
        }
    }

    @Override
    public void update(PermissionOperator operator) {
        if(operator.getId() == null) {
            throw BaseException.DB_UPDATE_NOT_INCLUDE_ID;
        }
        permissionOperatorRepository.save(operator);
    }

    @Override
    public PermissionOperator getById(Long operatorId) {
        return permissionOperatorRepository.findOne(operatorId);
    }

    @Override
    public PermissionOperator getByLoginName(String loginName) {
        return permissionOperatorRepository.getByLoginName(loginName);
    }

    @Override
    public void deleteById(Long operatorId) {
        permissionOperatorRepository.delete(operatorId);
    }

    @Override
    public void updatePassword(Long operatorId, String newPassword) {
        PermissionOperator operator = permissionOperatorRepository.findOne(operatorId);
        if (null != operator) {
            operator.setLoginPassword(bCryptPasswordEncoder.encode(operator.getLoginPassword().trim()));
            permissionOperatorRepository.save(operator);
        }
    }

    @Override
    public void saveOperatorAndRole(PermissionOperator permissionOperator, String roleString) {

    }

    @Override
    public void updateOperatorRole(PermissionOperator permissionOperator) {
        PermissionOperator operator = this.permissionOperatorRepository.findOne(permissionOperator.getId());
        if(operator == null) {
            throw BaseException.DB_UPDATE_NOT_INCLUDE_ID;
        }
        operator.getRoleSet().clear();
        operator.setRoleSet(permissionOperator.getRoleSet());
        this.permissionOperatorRepository.save(operator);
    }

    @Override
    public Page<PermissionOperator> getPage(int page, int pageSize) {
        Sort sort = new Sort(Sort.Direction.DESC,"createTime");
        Pageable pageable = new PageRequest(page, pageSize, sort);
        return this.permissionOperatorRepository.findAll(pageable);
    }
}
