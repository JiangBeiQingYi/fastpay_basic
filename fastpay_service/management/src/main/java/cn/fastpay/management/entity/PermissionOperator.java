package cn.fastpay.management.entity;

import cn.fastpay.core.entity.BaseObject;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * 权限管理-操作员
 * @author freewolf
 */
@Entity
@SQLDelete(sql = "UPDATE permission_operator SET deleted = 1 WHERE id = ?")
@Where(clause = "deleted = 0")
public class PermissionOperator extends BaseObject {

    private static final long serialVersionUID = 5448136936886215211L;
    /**
     * 登录名
     */
    @Column(unique = true, nullable = false, length = 32)
    private String loginName;
    /**
     * 登录密码
     */
    @Column(nullable = false, length = 64)
    private String loginPassword;
    /**
     * 姓名
     */
    @Column(nullable = false, length = 16)
    private String realName;
    /**
     * 手机号
     */
    @Column(nullable = false, length = 16)
    private String mobile;
    /**
     * 账号未被锁定
     */
    @Column(columnDefinition="tinyint(1) default 0")
    private Boolean accountNonLocked;
    /**
     * 操作员类型（admin:超级管理员，common:普通操作员），超级管理员由系统初始化时添加，不能删除
     */
    private String type;
    /**
     * 盐
     */
    private String salt;
    /**
     * 操作员对应的角色
     */
    @ManyToMany(cascade = {CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @JoinTable(name = "permission_operator_role", joinColumns = { @JoinColumn(name = "operator_id") },
            inverseJoinColumns = {@JoinColumn(name = "role_id") })
    private Set<PermissionRole> roleSet;

    public PermissionOperator() {
        this.accountNonLocked = false;
        this.roleSet = new HashSet<>();
    }

    @Override
    public String toString() {
        return "PermissionOperator{" +
                "loginName='" + loginName + '\'' +
                ", loginPassword='" + loginPassword + '\'' +
                ", realName='" + realName + '\'' +
                ", mobile='" + mobile + '\'' +
                ", accountNonLocked=" + accountNonLocked +
                ", type='" + type + '\'' +
                ", salt='" + salt + '\'' +
                ", roleSet=" + roleSet +
                '}';
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Boolean getAccountNonLocked() {
        return accountNonLocked;
    }

    public void setAccountNonLocked(Boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Set<PermissionRole> getRoleSet() {
        return roleSet;
    }

    public void setRoleSet(Set<PermissionRole> roleSet) {
        this.roleSet = roleSet;
    }
}
