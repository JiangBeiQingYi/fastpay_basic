package cn.fastpay.management.service;

import cn.fastpay.management.entity.PermissionOperator;
import org.springframework.data.domain.Page;

/**
 * @author freewolf
 */
public interface PermissionOperatorService {
    /**
     * 创建 PermissionOperator
     * @param permissionOperator
     */
    void add(PermissionOperator permissionOperator);

    /**
     * 修改 PermissionOperator
     * @param permissionOperator
     */
    void update(PermissionOperator permissionOperator);

    /**
     * 根据id获取数据 PermissionOperator
     *
     * @param id
     *
     * @return
     */
    PermissionOperator getById(Long id);

    /**
     * 根据登录名取得操作员对象
     */
    PermissionOperator getByLoginName(String loginName);

    /**
     * 根据ID删除一个操作员，同时删除与该操作员关联的角色关联信息. type="admin"的超级管理员不能删除.
     *
     * @param id
     * 操作员ID.
     */
    void deleteById(Long id);

    /**
     * 根据操作员ID更新操作员密码.
     *
     * @param operatorId
     * @param newPwd
     * (已进行SHA1加密)
     */
    void updatePassword(Long operatorId, String newPwd);

    /**
     * 保存操作員信息及其关联的角色.
     *
     * @param permissionOperator
     *            .
     * @param roleString
     *            .
     */
    void saveOperatorAndRole(PermissionOperator permissionOperator, String roleString);

    /**
     * 修改操作員信息及其关联的角色.
     *
     * @param permissionOperator
     *            .
     */
    void updateOperatorRole(PermissionOperator permissionOperator);

    /**
     * 获取操作员分页
     *
     * @param page 页码
     *
     * @param pageSize
     *
     * @return
     */
    Page<PermissionOperator> getPage(int page, int pageSize);
}
