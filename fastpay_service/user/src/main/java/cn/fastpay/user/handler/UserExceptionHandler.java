package cn.fastpay.user.handler;

import cn.fastpay.core.handler.GlobalExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author freewolf
 */
@RestControllerAdvice
public class UserExceptionHandler extends GlobalExceptionHandler {
}
