package cn.fastpay.user.security;

import cn.fastpay.core.security.UserJwtAuthorizationFilter;
import org.springframework.security.authentication.AuthenticationManager;

/**
 * @author freewolf
 */
public class JwtAuthorizationFilter extends UserJwtAuthorizationFilter {

    public JwtAuthorizationFilter(AuthenticationManager authManager) {
        super(authManager);
    }
}
