package cn.fastpay.user;

import cn.fastpay.core.exception.BaseException;
import cn.fastpay.core.util.ErrorUtil;
import cn.fastpay.user.entity.User;
import cn.fastpay.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.DefaultErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.context.request.RequestAttributes;

import java.util.HashMap;
import java.util.Map;

/**
 * @author freewolf
 */
@SpringBootApplication
public class UserApplication implements CommandLineRunner {

    @Autowired
    private UserService userService;

    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class, args);
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public ErrorAttributes errorAttributes() {
        return new DefaultErrorAttributes() {
            @Override
            public Map<String, Object> getErrorAttributes(RequestAttributes requestAttributes, boolean includeStackTrace) {
                Map<String, Object> errorAttributes = super.getErrorAttributes(requestAttributes, includeStackTrace);
                return ErrorUtil.getErrorAttributes(errorAttributes, this, requestAttributes, includeStackTrace);
            }
        };
    }

    @Override
    public void run(String... strings) throws Exception {
        User user = new User();
        user.setUsername("yongkang");
        user.setPassword("findlove");
        user.setRealName("永康");
        user.setEnabled(true);

//        this.userService.save(user);
    }
}
