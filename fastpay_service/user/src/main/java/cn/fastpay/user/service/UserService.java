package cn.fastpay.user.service;

import cn.fastpay.user.entity.User;

/**
 * @author freewolf
 */
public interface UserService {

    void save(User user);

    User findByUsername(String username);
}
