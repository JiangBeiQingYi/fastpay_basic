package cn.fastpay.user.entity;


import cn.fastpay.core.entity.BaseObject;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;

/**
 * @author freewolf
 */
@Entity
public class User extends BaseObject {

    private String username;

    @JsonIgnore
    private String password;

    private String realName;

    private boolean enabled;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
