package cn.fastpay.user.exception;

import cn.fastpay.core.exception.BaseException;

/**
 * @author freewolf
 */
public class UserException {
    public static final BaseException USER_PASSWORD_WRONG = new BaseException(
            10030001, "账号不存在或者密码错误!");
    public static final BaseException USER_LOCKED = new BaseException(
            10030002, "该操作员账号被锁定!");
    public static final BaseException USER_NOT_FOUND = new BaseException(
            10030003, "该操作员未找到!");
}
