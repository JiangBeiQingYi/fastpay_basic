package cn.fastpay.user.controller;


import cn.fastpay.core.util.AuthUtil;
import cn.fastpay.core.vo.ApiCommonResultVO;
import cn.fastpay.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author freewolf
 */
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/user/info")
    public Object findByUsername() {
        return new ApiCommonResultVO(this.userService.findByUsername(AuthUtil.getUsername()));
    }
}
