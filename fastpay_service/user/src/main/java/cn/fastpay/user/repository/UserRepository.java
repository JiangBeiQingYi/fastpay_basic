package cn.fastpay.user.repository;

import cn.fastpay.user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author freewolf
 */
public interface UserRepository extends JpaRepository<User, Integer> {

    /**
     * @param username
     * @return
     */
    User findByUsername(String username);
}
