package cn.fastpay.paydemo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PayController {

    @GetMapping("/pay/balance")
    public Object getBalance() {
        return "1000.22";
    }
}
