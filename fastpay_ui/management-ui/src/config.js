export default {
  SYS_NAME: 'VUEADMIN',
  KEY_USER_TOKEN: 'userToken',
  JWT_HEADER: 'Authorization',
  JWT_PERFIX: 'Bearer ',
  KEY_OPERATOR: 'operator',
  PAGE_SIZE: 10
}
