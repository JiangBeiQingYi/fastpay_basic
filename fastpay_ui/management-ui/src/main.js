// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import VueCookie from 'vue-cookie'
import Api from './api'
import Config from './config'
import 'font-awesome/css/font-awesome.min.css'

Vue.config.productionTip = false

Vue.use(ElementUI)
// Cookie
Vue.use(VueCookie)
axios.defaults.headers.common['Content-Type'] = 'application/json'

// 总线bus
Vue.prototype.$bus = new Vue()
// 装载组件
Vue.prototype.$axios = axios
Vue.prototype.$api = Api
Vue.prototype.$config = Config

Vue.directive('can', {
  inserted: function (el, binding, vnode) {
    var permissions = JSON.parse(binding.expression)
    console.log('has directive', permissions)

    var permissionList = window.operator.permissionList
    if (Array.isArray(permissionList)) {
      if (!permissionList.includes(permissions[0])) {
        // vnode.context.$parent.$vnode.removeChild(vnode.elm)
        el.parentNode.removeChild(el)
      }
    }
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
