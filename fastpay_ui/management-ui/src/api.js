// const API_PERFIX = '/admin'
// const SERVER = 'http://locakhost:9000' + API_PERFIX

const API_PERFIX = ''
const SERVER = 'http://localhost:9000' + API_PERFIX

export default {
  LOGIN: { url: SERVER + '/api/operator/login' },
  // 操作员
  OPERATOR_INFO: { url: SERVER + '/api/operator/info' },
  OPERATOR_PAGE: { url: SERVER + '/api/operator/page' },
  OPERATOR_ADD: { url: SERVER + '/api/operator/add' },
  OPERATOR_UPDATE: { url: SERVER + '/api/operator/update' },
  OPERATOR_UPDATE_ROLE: { url: SERVER + '/api/operator/update/role' },
  OPERATOR_REMOVE: { url: SERVER + '/api/operator/remove' },
  OPERATOR_LOCK: { url: SERVER + '/api/operator/lock' },
  // 权限
  ACTION_PAGE: { url: SERVER + '/api/action/page' },
  ACTION_ALL: { url: SERVER + '/api/action/all' },
  ACTION_ADD: { url: SERVER + '/api/action/add' },
  ACTION_UPDATE: { url: SERVER + '/api/action/update' },
  ACTION_REMOVE: { url: SERVER + '/api/action/remove' },
  // 菜单
  MENU_ALL: { url: SERVER + '/api/menu/all' },
  MENU_ADD: { url: SERVER + '/api/menu/add' },
  MENU_UPDATE: { url: SERVER + '/api/menu/update' },
  MENU_REMOVE: { url: SERVER + '/api/menu/remove' },
  // 角色
  ROLE_PAGE: { url: SERVER + '/api/role/page' },
  ROLE_ADD: { url: SERVER + '/api/role/add' },
  ROLE_ALL: { url: SERVER + '/api/role/all' },
  ROLE_UPDATE: { url: SERVER + '/api/role/update' },
  ROLE_UPDATE_ACTION: { url: SERVER + '/api/role/update/action' },
  ROLE_REMOVE: { url: SERVER + '/api/role/remove' }
}
