export default {
  setOperator: function (app, operator) {
    var currentOperator = {
      loginName: operator.loginName,
      realName: operator.realName
    }
    currentOperator.menuList = []
    currentOperator.actionList = []
    // 权限列表
    currentOperator.roleList = []
    currentOperator.permissionList = []

    for (var i in operator.roleSet) {
      var role = operator.roleSet[i]
      currentOperator.roleList.push({ role: role.role, name: role.roleName })
      currentOperator.permissionList.push(role.role)
      for (var j in role.menuSet) {
        currentOperator.menuList.push(role.menuSet[j])
      }
      for (var k in role.actionSet) {
        currentOperator.actionList.push({ permission: role.actionSet[k].permission, name: role.actionSet[k].permissionName })
        currentOperator.permissionList.push(role.actionSet[k].permission)
      }
    }
    // 排序
    currentOperator.menuList.sort((x, y) => x.sort < y.sort)
    for (var x in currentOperator.menuList) {
      var menu = currentOperator.menuList[x]
      if (menu.parent !== null) {
        var parent = currentOperator.menuList.find(x => x.id === menu.parent.id)
        if (!parent.hasOwnProperty('submenu')) {
          parent.submenu = []
        }
        parent.submenu.push(menu)
      }
    }
    // 过滤掉二级菜单
    currentOperator.menuList = currentOperator.menuList.filter(menu => menu.parent === null)
    console.log('operator', currentOperator)
    app.$operator = currentOperator
    return currentOperator
  }
}
